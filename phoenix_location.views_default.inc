<?php
/**
 * @file
 * phoenix_location.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function phoenix_location_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'location';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'eck_compro_component';
  $view->human_name = 'Location';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Headquarters';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'entity';
  /* Field: Component: Id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'eck_compro_component';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  /* Sort criterion: Component: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'eck_compro_component';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Filter criterion: Component: compro_component type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_compro_component';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'location' => 'location',
  );
  /* Filter criterion: Component: Headquarters (field_location_headquarters) */
  $handler->display->display_options['filters']['field_location_headquarters_value']['id'] = 'field_location_headquarters_value';
  $handler->display->display_options['filters']['field_location_headquarters_value']['table'] = 'field_data_field_location_headquarters';
  $handler->display->display_options['filters']['field_location_headquarters_value']['field'] = 'field_location_headquarters_value';
  $handler->display->display_options['filters']['field_location_headquarters_value']['value'] = array(
    1 => '1',
  );

  /* Display: Headquarters */
  $handler = $view->new_display('block', 'Headquarters', 'headquarters');
  $handler->display->display_options['defaults']['title'] = FALSE;

  /* Display: All Locations */
  $handler = $view->new_display('block', 'All Locations', 'all_locations');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Component: compro_component type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_compro_component';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'location' => 'location',
  );
  /* Filter criterion: Component: Headquarters (field_location_headquarters) */
  $handler->display->display_options['filters']['field_location_headquarters_value']['id'] = 'field_location_headquarters_value';
  $handler->display->display_options['filters']['field_location_headquarters_value']['table'] = 'field_data_field_location_headquarters';
  $handler->display->display_options['filters']['field_location_headquarters_value']['field'] = 'field_location_headquarters_value';
  $handler->display->display_options['filters']['field_location_headquarters_value']['operator'] = 'not';
  $handler->display->display_options['filters']['field_location_headquarters_value']['value'] = array(
    1 => '1',
  );
  $export['location'] = $view;

  return $export;
}
