<?php
/**
 * @file
 * phoenix_location.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function phoenix_location_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_phone_numbers|compro_component|location|form';
  $field_group->group_name = 'group_phone_numbers';
  $field_group->entity_type = 'compro_component';
  $field_group->bundle = 'location';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Phone Numbers',
    'weight' => '4',
    'children' => array(
      0 => 'field_location_local_phone',
      1 => 'field_location_phone_number',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-phone-numbers field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_phone_numbers|compro_component|location|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_text|compro_component|location|default';
  $field_group->group_name = 'group_text';
  $field_group->entity_type = 'compro_component';
  $field_group->bundle = 'location';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Text',
    'weight' => '1',
    'children' => array(
      0 => 'field_location_address',
      1 => 'field_location_e_mail',
      2 => 'field_location_local_phone',
      3 => 'field_location_phone_number',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Text',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-text field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_text|compro_component|location|default'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Phone Numbers');
  t('Text');

  return $field_groups;
}
