<?php
/**
 * @file
 * phoenix_location.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function phoenix_location_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'eck add compro_component location entities'.
  $permissions['eck add compro_component location entities'] = array(
    'name' => 'eck add compro_component location entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck administer compro_component location entities'.
  $permissions['eck administer compro_component location entities'] = array(
    'name' => 'eck administer compro_component location entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck delete compro_component location entities'.
  $permissions['eck delete compro_component location entities'] = array(
    'name' => 'eck delete compro_component location entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck edit compro_component location entities'.
  $permissions['eck edit compro_component location entities'] = array(
    'name' => 'eck edit compro_component location entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck list compro_component location entities'.
  $permissions['eck list compro_component location entities'] = array(
    'name' => 'eck list compro_component location entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck view compro_component location entities'.
  $permissions['eck view compro_component location entities'] = array(
    'name' => 'eck view compro_component location entities',
    'roles' => array(),
    'module' => 'eck',
  );

  return $permissions;
}
