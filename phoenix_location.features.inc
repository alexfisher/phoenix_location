<?php
/**
 * @file
 * phoenix_location.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function phoenix_location_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function phoenix_location_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function phoenix_location_eck_bundle_info() {
  $items = array(
    'compro_component_location' => array(
      'machine_name' => 'compro_component_location',
      'entity_type' => 'compro_component',
      'name' => 'location',
      'label' => 'Location',
      'config' => array(
        'managed_properties' => array(
          'title' => 'title',
          'language' => 0,
        ),
      ),
    ),
  );
  return $items;
}
