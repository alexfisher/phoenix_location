<?php
/**
 * @file
 * phoenix_location.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function phoenix_location_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-location-all_locations'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'location-all_locations',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'compro_adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'compro_adminimal',
        'weight' => 0,
      ),
      'compro_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'compro_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-location-headquarters'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'location-headquarters',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'compro_adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'compro_adminimal',
        'weight' => 0,
      ),
      'compro_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'compro_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
